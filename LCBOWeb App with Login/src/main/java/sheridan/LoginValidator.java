package sheridan;

public class LoginValidator {

	public static boolean isValidLoginName( String loginName ) {
		String lName = loginName;
		int count = 0;
		for (int i =0; i < lName.length(); i++)
		{
			if (Character.isDigit(lName.charAt(i)))
			{
				count++;
			}
		}
		if (count >= 1 && lName.length() >= 6 && Character.isDigit(lName.charAt(0)) == false )
		{
			return true;
		}
		else {
			return false;
		}
		
	}
}
